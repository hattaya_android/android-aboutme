package buu.hattaya.aboutme

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import buu.hattaya.aboutme.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private  val myName: MyName = MyName("Hattaya Nakchookeaw")
    lateinit var myButton: Button
    lateinit var edittext: EditText
    lateinit var nicknametextView: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main)
        binding.apply {
            myButton = doneButton
            edittext = nicknameEdit
            nicknametextView = nicknameText

            this.myName = this@MainActivity.myName
        }

        myButton.setOnClickListener {
            addNickname(it)
        }

        nicknametextView.setOnClickListener {
            updateNickname(it)
        }


    }

     private fun addNickname(v: View) {
         myButton.visibility = View.GONE
         edittext.visibility = View.GONE
         nicknametextView.visibility = View.VISIBLE
         binding.apply {
             myName?.nickname = edittext.text.toString()
             invalidateAll()
         }
         val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
         inputMethodManager.hideSoftInputFromWindow(v.windowToken,0)
    }

    private fun updateNickname(v: View) {
        edittext.visibility = View.VISIBLE
        myButton.visibility = View.VISIBLE
        v.visibility = View.GONE
        edittext.requestFocus()
        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.showSoftInput(edittext,0)
    }
}